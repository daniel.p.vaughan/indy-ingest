service: indy-ingest

provider:
  name: aws
  runtime: go1.x
  environment:
    DATASET_TABLE: "${self:service}-${opt:stage, self:provider.stage}-dataset"
    METADATA_TABLE: "${self:service}-${opt:stage, self:provider.stage}-metadata"
    METADATA_BUCKET: "${self:service}-${opt:stage, self:provider.stage}-metadata"
  iamRoleStatements:
    - Effect: Allow
      Action:
        - dynamodb:GetItem
        - dynamodb:PutItem
      Resource: "arn:aws:dynamodb:${opt:region, self:provider.region}:*:table/${self:provider.environment.DATASET_TABLE}"
    - Effect: Allow
      Action:
        - dynamodb:GetItem
        - dynamodb:PutItem
      Resource: "arn:aws:dynamodb:${opt:region, self:provider.region}:*:table/${self:provider.environment.METADATA_TABLE}"
    - Effect: Allow
      Action:
        - s3:GetObject
        - s3:PutObject
      Resource: "arn:aws:s3:::${self:provider.environment.METADATA_BUCKET}/*"

package:
 exclude:
   - ./**
 include:
   - ./bin/**

functions:
  create-dataset:
    handler: bin/create_dataset
    events:
      - http:
          path: dataset
          method: post
  list-datasets:
    handler: bin/dummy
    events:
      - http:
          path: dataset
          method: get
  get-dataset:
    handler: bin/dummy
    events:
      - http:
          path: dataset/{id}
          method: get
          request:
            parameters:
              paths:
              id: true
  add-metadata:
    handler: bin/add_metadata
    events:
      - http:
          path: dataset/{id}/metadata
          method: post
          request:
            parameters:
              paths:
              id: true
  list-metadata:
    handler: bin/dummy
    events:
      - http:
          path: dataset/{id}/metadata
          method: get
          request:
            parameters:
              paths:
              id: true
  get-metadata:
    handler: bin/dummy
    events:
      - http:
          path: dataset/{id}/metadata/{uuid}
          method: get
          request:
            parameters:
              paths:
                id: true
                uuid: true
  validate:
    handler: bin/validate
    events:
    - http:
        path: validate
        method: post
        cors:
          origin: '*'
          headers:
            - Content-Type
            - Link
            - X-Amz-Date
            - Authorization
            - X-Api-Key
            - X-Amz-Security-Token
            - X-Amz-User-Agent
          allowCredentials: false

resources:
  Resources:
    DatasetDynamoDbTable:
      Type: AWS::DynamoDB::Table
      Properties:
        TableName: ${self:provider.environment.DATASET_TABLE}
        AttributeDefinitions:
          - AttributeName: owner
            AttributeType: S
          - AttributeName: dataset_id
            AttributeType: S
        KeySchema:
          - AttributeName: owner
            KeyType: HASH
          - AttributeName: dataset_id
            KeyType: RANGE
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1
    MetadataDynamoDbTable:
      Type: AWS::DynamoDB::Table
      Properties:
        TableName: ${self:provider.environment.METADATA_TABLE}
        AttributeDefinitions:
          - AttributeName: dataset_id
            AttributeType: S
          - AttributeName: metadata_id
            AttributeType: S
        KeySchema:
          - AttributeName: dataset_id
            KeyType: HASH
          - AttributeName: metadata_id
            KeyType: RANGE
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1
    MetadataBucket:
      Type: AWS::S3::Bucket
      Properties:
        BucketName: ${self:provider.environment.METADATA_BUCKET}
        AccessControl: Private
    S3BucketPermissions:
      Type: AWS::S3::BucketPolicy
      Properties:
        Bucket: ${self:provider.environment.METADATA_BUCKET}
        PolicyDocument:
          Statement:
            - Principal: "*"
              Action:
                - s3:GetObject
              Effect: Allow
              Sid: "AddPerm"
              Resource: "arn:aws:s3:::${self:provider.environment.METADATA_BUCKET}/*"
